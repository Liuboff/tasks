Array.prototype.myForEach = function(arr, fn) {
  if (!Array.isArray(arr) || arr.length == 0 || typeof fn !== "function") {
    return [];
  } else {
    for (let i = 0; i < arr.length; i++) {
      fn(arr[i], i, arr);
    }
  }
}

Array.prototype.myMap = function(arr, fn) {
  if (!Array.isArray(arr) || arr.length == 0 || typeof fn !== "function") {
    return [];
  } else {
    let newArr = [];
    for (let i = 0; i < arr.length; i++) {
      newArr.push(fn(arr[i], i, arr));
    }
    return newArr;
  }
}

Array.prototype.mySort = function(arr, fn) {
  if (!Array.isArray(arr) || arr.length == 0) {
    return [];
  } else {
    let newArr = [...arr];
    if (typeof fn == "function") {
      for (let i = 0; i < newArr.length; i++) {
        for (let j = 0; j < newArr.length - i; j++) {
            if (fn(newArr[j], newArr[j+1]) > 0) {
                [newArr[j], newArr[j + 1]] = [newArr[j + 1], newArr[j]];
            } else if ((fn(newArr[j], newArr[j+1]) < 0)) {
                continue;
            } else {
                continue;
            }
        }
      }
    } else {
      for (let i = 0; i < newArr.length; i++) {
        for (let j = 0; j < newArr.length - i; j++) {
          if (String(newArr[j]) > String(newArr[j + 1])) {
            [newArr[j], newArr[j + 1]] = [newArr[j + 1], newArr[j]];
          }
        }
      }
    }
    return newArr;
  }
}

Array.prototype.myFilter = function(arr, fn) {
  if (!Array.isArray(arr) || arr.length == 0 || typeof fn !== "function") {
    return [];
  } else {
    let newArr = [];
    for (let i = 0; i < arr.length; i++) {
      if (fn(arr[i], i, arr)) {
        newArr.push(arr[i]);
      }
    }
    return newArr;
  }
}

Array.prototype.myFind = function (arr, fn) {
  if (!Array.isArray(arr) || arr.length == 0 || typeof fn !== "function") {
    return;
  } else {
    for (let i = 0; i < arr.length; i++) {
      if (fn(arr[i], i, arr)) {
        return arr[i];
      }
    }
  }
};

// let array = [72, -4, 6, 112, -2, 15, 6];

// array.myForEach(array, (elem, index, arr) => console.log( elem * 2 ) );

// console.log(array.myMap(array, (elem, index, arr) => elem * 2 ));

// console.log(array.mySort(array, (a, b) => {
//   if (a > b) return 1;
//   if (a == b) return 0;
//   if (a < b) return -1;
// }));
// console.log(array.mySort(array));

// console.log(array.myFilter(array, (a) => {return a < 0}));

// console.log(array.myFind(array, (a) => {return a < 0}));

// console.log(array);
