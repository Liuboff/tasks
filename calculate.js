function calculate(a, b) {
    let res = a + b;
    function inner() {
      return inner;
    }
    inner.add = function (z) {
      res += z;
      return inner;
    }
    inner.minus = function (z) {
      res -= z;
      return inner;
    }
    inner.multiply = function (z) {
      res *= z;
      return inner;
    }
    inner.divide = function (z) {
      res /= z;
      return inner;
    }
    inner.result = function () {
      return res;
    }
    return inner;
  }
