function sum(x) {
  return (y) => {
    if (typeof y === "undefined") {
      return x;
    }

    return sum(x + y);
  };
}


// function curry(func) {
//   return function curried(...args) {
//     if (args.length >= func.length) {
//       return func.apply(this, args);
//     } else {
//       return function (...args2) {
//         return curried.apply(this, args.concat(args2));
//       };
//     }
//   };
// }
// function sum(a, b, c, d, f) {
//   return a + b + c + d + f;
// }
// let curriedSum = curry(sum);
// console.log(curriedSum(2)(3)(4)(1)(-1));
