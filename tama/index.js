let header = document.getElementById("header");

let buttons = {
  feedBtn: document.querySelector("#feed"),
  sleepdBtn: document.querySelector("#sleep"),
  walkBtn: document.querySelector("#walk"),
  hygieneBtn: document.querySelector("#hygiene"),
  socialBtn: document.querySelector("#social")
}

let body = document.querySelector("body");
let message = document.querySelector("#message");
let pet = document.querySelector("#pet");
let mouth = document.querySelector("#mouth");

let info = {
  satietyInfo: document.getElementById("satietyInfo"),
  sleepInfo: document.getElementById("sleepInfo"),
  happinessInfo: document.getElementById("happinessInfo"),
  hygieneInfo: document.getElementById("hygieneInfo"),
  socialInfo: document.getElementById("socialInfo"),
  healthInfo: document.getElementById("health")
}

let enterName = '';

class Tama {
  runTimer;
  static tick = 2;

  constructor(enterName) {
    this.name = enterName;
    this.happiness = 100;
    this.satiety = 100;
    this.sleeping = 100;
    this.hygiene = 100;
    this.social = 100;
    this.health = this.healthCare();
    this.isAlive = false;
    this.runTimer = null;
  }

  walk() {
    this.happiness = 100;
    info.happinessInfo.innerHTML = `${this.happiness}%`;
  }

  feed() {
    this.satiety = 100;
    info.satietyInfo.innerHTML = `${this.satiety}%`;
  }

  sleep() {
    this.sleeping = 100;
    info.sleepInfo.innerHTML = `${this.sleeping}%`;
  }

  bath() {
    this.hygiene = 100;
    info.hygieneInfo.innerHTML = `${this.hygiene}%`;
  }

  communicate() {
    this.social = 100;
    info.socialInfo.innerHTML = `${this.social}%`;
  }

  healthCare() {
    return (
      this.happiness + this.satiety + this.sleeping + this.hygiene + this.social
    );
  }

  ticking() {
    this.satiety -= Tama.tick;
    this.happiness -= Tama.tick;
    this.sleeping -= Tama.tick;
    this.hygiene -= Tama.tick;
    this.social -= Tama.tick;
    this.health = this.healthCare();

    this.changeHtml();
  }

  changeHtml() {
    info.happinessInfo.innerHTML = `${this.happiness}%`;
    info.satietyInfo.innerHTML = `${this.satiety}%`;
    info.sleepInfo.innerHTML = `${this.sleeping}%`;
    info.hygieneInfo.innerHTML = `${this.hygiene}%`;
    info.socialInfo.innerHTML = `${this.social}%`;
    info.healthInfo.style.width = `${this.health}px`;
    info.healthInfo.innerHTML = `${this.social}%`;
  }

  changeColor(color) {
    pet.style.backgroundColor = color;
  }

  start() {
    this.isAlive = true;
    this.changeColor('green');
    
    header.innerHTML = this.name;
    buttons.feedBtn.addEventListener("click", ()=>{this.feed()});
    buttons.sleepdBtn.addEventListener("click", ()=>{this.sleep()});
    buttons.walkBtn.addEventListener("click", ()=>{this.walk()});
    buttons.hygieneBtn.addEventListener("click", ()=>{this.bath()});
    buttons.socialBtn.addEventListener("click", ()=>{this.communicate()});

    this.runTimer = setInterval(() => {
      this.ticking();
      if (this.satiety <= 0 ||
        this.happiness <= 0 ||
        this.sleeping <= 0 ||
        this.hygiene <= 0 ||
        this.social <= 0) {
          this.end();
      } else {
        if (this.health > 300 && this.health <= 400) {
          this.changeColor('orange');
        }
        if (this.health > 200 && this.health <= 300) {
          this.changeColor('yellow');
        }
        if (this.health > 100 && this.health <= 200) {
          this.changeColor('red');
        }
      }
    }, 1000);

  }

  end() {
    this.isAlive = false;

    this.satiety = 0;
    this.happiness = 0;
    this.sleeping = 0;
    this.hygiene = 0;
    this.social = 0;
    this.health = this.healthCare();
    this.changeHtml();

    clearInterval(this.runTimer);
    body.style.backgroundColor = "gray";
    mouth.classList = 'dead';
    this.changeColor('black');
    message.innerHTML = `Your pet ${this.name} is dead :(`;
  }
}

enterName = prompt("Enter the pet name:");
const tamaInstance = new Tama(enterName);
tamaInstance.start();
